﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void Start_Form_Load(object sender, EventArgs e)
        {

        }

        private void Return_button_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
            
        }

        private void Next_button_Click(object sender, EventArgs e)
        {
            Choose_service cs = new Choose_service();
            this.Hide();
            cs.ShowDialog();
            if(cs.DialogResult== DialogResult.OK)
            {
                this.Show();
            }
        }
    }
}
