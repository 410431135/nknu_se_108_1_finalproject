﻿namespace WindowsFormsApp1
{
    partial class bill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_label = new System.Windows.Forms.Label();
            this.Phone_label = new System.Windows.Forms.Label();
            this.ID_label = new System.Windows.Forms.Label();
            this.Credit_Card_label = new System.Windows.Forms.Label();
            this.Room_Number_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Discription_label = new System.Windows.Forms.Label();
            this.Over_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(212, 95);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(41, 12);
            this.Name_label.TabIndex = 0;
            this.Name_label.Text = "姓名：";
            // 
            // Phone_label
            // 
            this.Phone_label.AutoSize = true;
            this.Phone_label.Location = new System.Drawing.Point(214, 131);
            this.Phone_label.Name = "Phone_label";
            this.Phone_label.Size = new System.Drawing.Size(41, 12);
            this.Phone_label.TabIndex = 1;
            this.Phone_label.Text = "電話：";
            // 
            // ID_label
            // 
            this.ID_label.AutoSize = true;
            this.ID_label.Location = new System.Drawing.Point(216, 167);
            this.ID_label.Name = "ID_label";
            this.ID_label.Size = new System.Drawing.Size(77, 12);
            this.ID_label.TabIndex = 2;
            this.ID_label.Text = "身分證字號：";
            // 
            // Credit_Card_label
            // 
            this.Credit_Card_label.AutoSize = true;
            this.Credit_Card_label.Location = new System.Drawing.Point(218, 210);
            this.Credit_Card_label.Name = "Credit_Card_label";
            this.Credit_Card_label.Size = new System.Drawing.Size(65, 12);
            this.Credit_Card_label.TabIndex = 3;
            this.Credit_Card_label.Text = "信用卡號：";
            // 
            // Room_Number_label
            // 
            this.Room_Number_label.AutoSize = true;
            this.Room_Number_label.Location = new System.Drawing.Point(220, 246);
            this.Room_Number_label.Name = "Room_Number_label";
            this.Room_Number_label.Size = new System.Drawing.Size(65, 12);
            this.Room_Number_label.TabIndex = 4;
            this.Room_Number_label.Text = "預約房號：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(260, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(262, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(290, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(292, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "label5";
            // 
            // Discription_label
            // 
            this.Discription_label.AutoSize = true;
            this.Discription_label.Location = new System.Drawing.Point(102, 50);
            this.Discription_label.Name = "Discription_label";
            this.Discription_label.Size = new System.Drawing.Size(257, 12);
            this.Discription_label.TabIndex = 10;
            this.Discription_label.Text = "預約成功！以下是您的訂房資料，請妥善保管。";
            // 
            // Over_button
            // 
            this.Over_button.Location = new System.Drawing.Point(361, 357);
            this.Over_button.Name = "Over_button";
            this.Over_button.Size = new System.Drawing.Size(75, 23);
            this.Over_button.TabIndex = 11;
            this.Over_button.Text = "結束";
            this.Over_button.UseVisualStyleBackColor = true;
            this.Over_button.Click += new System.EventHandler(this.Over_button_Click);
            // 
            // bill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Over_button);
            this.Controls.Add(this.Discription_label);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Room_Number_label);
            this.Controls.Add(this.Credit_Card_label);
            this.Controls.Add(this.ID_label);
            this.Controls.Add(this.Phone_label);
            this.Controls.Add(this.Name_label);
            this.Name = "bill";
            this.Text = "帳單資訊";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.Label Phone_label;
        private System.Windows.Forms.Label ID_label;
        private System.Windows.Forms.Label Credit_Card_label;
        private System.Windows.Forms.Label Room_Number_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Discription_label;
        private System.Windows.Forms.Button Over_button;
    }
}