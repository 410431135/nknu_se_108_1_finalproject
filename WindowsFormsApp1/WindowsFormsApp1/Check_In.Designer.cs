﻿namespace WindowsFormsApp1
{
    partial class Check_In
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Discription_label = new System.Windows.Forms.Label();
            this.Name_label = new System.Windows.Forms.Label();
            this.ID_label = new System.Windows.Forms.Label();
            this.Room_Number_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Next_button = new System.Windows.Forms.Button();
            this.Return_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Discription_label
            // 
            this.Discription_label.AutoSize = true;
            this.Discription_label.Location = new System.Drawing.Point(104, 60);
            this.Discription_label.Name = "Discription_label";
            this.Discription_label.Size = new System.Drawing.Size(329, 12);
            this.Discription_label.TabIndex = 0;
            this.Discription_label.Text = "以下是您的訂房紀錄，請確認是否正確，若錯誤請重新訂房：";
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(203, 99);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(41, 12);
            this.Name_label.TabIndex = 1;
            this.Name_label.Text = "姓名：";
            // 
            // ID_label
            // 
            this.ID_label.AutoSize = true;
            this.ID_label.Location = new System.Drawing.Point(167, 132);
            this.ID_label.Name = "ID_label";
            this.ID_label.Size = new System.Drawing.Size(77, 12);
            this.ID_label.TabIndex = 2;
            this.ID_label.Text = "身分證字號：";
            // 
            // Room_Number_label
            // 
            this.Room_Number_label.AutoSize = true;
            this.Room_Number_label.Location = new System.Drawing.Point(179, 160);
            this.Room_Number_label.Name = "Room_Number_label";
            this.Room_Number_label.Size = new System.Drawing.Size(65, 12);
            this.Room_Number_label.TabIndex = 3;
            this.Room_Number_label.Text = "預約房號：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(251, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "label3";
            // 
            // Next_button
            // 
            this.Next_button.Location = new System.Drawing.Point(409, 300);
            this.Next_button.Name = "Next_button";
            this.Next_button.Size = new System.Drawing.Size(75, 23);
            this.Next_button.TabIndex = 7;
            this.Next_button.Text = "確認";
            this.Next_button.UseVisualStyleBackColor = true;
            this.Next_button.Click += new System.EventHandler(this.Next_button_Click);
            // 
            // Return_button
            // 
            this.Return_button.Location = new System.Drawing.Point(281, 300);
            this.Return_button.Name = "Return_button";
            this.Return_button.Size = new System.Drawing.Size(75, 23);
            this.Return_button.TabIndex = 8;
            this.Return_button.Text = "刪除紀錄";
            this.Return_button.UseVisualStyleBackColor = true;
            // 
            // Check_In
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Return_button);
            this.Controls.Add(this.Next_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Room_Number_label);
            this.Controls.Add(this.ID_label);
            this.Controls.Add(this.Name_label);
            this.Controls.Add(this.Discription_label);
            this.Name = "Check_In";
            this.Text = "確認訂房資訊";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Discription_label;
        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.Label ID_label;
        private System.Windows.Forms.Label Room_Number_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Next_button;
        private System.Windows.Forms.Button Return_button;
    }
}