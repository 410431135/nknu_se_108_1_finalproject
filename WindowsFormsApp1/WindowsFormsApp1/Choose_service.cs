﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Choose_service : Form
    {
        public Choose_service()
        {
            InitializeComponent();
        }

        private void Reservation_button_Click(object sender, EventArgs e)
        {
            Choose_Room cr = new Choose_Room();
            this.Hide();
            cr.ShowDialog();
            if (cr.DialogResult == DialogResult.OK)
            {
                this.Show();
            }
        }

        private void Check_In_button_Click(object sender, EventArgs e)
        {
            Check_In ci = new Check_In();
            ci.Show();
            this.Hide();
        }

        private void Return_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.DialogResult = DialogResult.OK;
        }
    }
}
