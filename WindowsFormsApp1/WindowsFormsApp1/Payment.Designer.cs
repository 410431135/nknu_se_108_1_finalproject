﻿namespace WindowsFormsApp1
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_label = new System.Windows.Forms.Label();
            this.ID_label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Description_label = new System.Windows.Forms.Label();
            this.Credit_Card_label = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Print_button = new System.Windows.Forms.Button();
            this.Return_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(193, 87);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(41, 12);
            this.Name_label.TabIndex = 0;
            this.Name_label.Text = "姓名：";
            // 
            // ID_label
            // 
            this.ID_label.AutoSize = true;
            this.ID_label.Location = new System.Drawing.Point(193, 114);
            this.ID_label.Name = "ID_label";
            this.ID_label.Size = new System.Drawing.Size(77, 12);
            this.ID_label.TabIndex = 1;
            this.ID_label.Text = "身分證字號：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(276, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "label4";
            // 
            // Description_label
            // 
            this.Description_label.AutoSize = true;
            this.Description_label.Location = new System.Drawing.Point(91, 52);
            this.Description_label.Name = "Description_label";
            this.Description_label.Size = new System.Drawing.Size(293, 12);
            this.Description_label.TabIndex = 4;
            this.Description_label.Text = "請確認您的個人資料，若正確請輸入您的信用卡資料：";
            // 
            // Credit_Card_label
            // 
            this.Credit_Card_label.AutoSize = true;
            this.Credit_Card_label.Location = new System.Drawing.Point(158, 185);
            this.Credit_Card_label.Name = "Credit_Card_label";
            this.Credit_Card_label.Size = new System.Drawing.Size(65, 12);
            this.Credit_Card_label.TabIndex = 5;
            this.Credit_Card_label.Text = "信用卡號：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(229, 182);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(368, 22);
            this.textBox1.TabIndex = 6;
            // 
            // Print_button
            // 
            this.Print_button.Location = new System.Drawing.Point(535, 309);
            this.Print_button.Name = "Print_button";
            this.Print_button.Size = new System.Drawing.Size(75, 23);
            this.Print_button.TabIndex = 7;
            this.Print_button.Text = "列印帳單";
            this.Print_button.UseVisualStyleBackColor = true;
            this.Print_button.Click += new System.EventHandler(this.Print_button_Click);
            // 
            // Return_button
            // 
            this.Return_button.Location = new System.Drawing.Point(442, 309);
            this.Return_button.Name = "Return_button";
            this.Return_button.Size = new System.Drawing.Size(75, 23);
            this.Return_button.TabIndex = 8;
            this.Return_button.Text = "返回";
            this.Return_button.UseVisualStyleBackColor = true;
            this.Return_button.Click += new System.EventHandler(this.Return_button_Click);
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Return_button);
            this.Controls.Add(this.Print_button);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Credit_Card_label);
            this.Controls.Add(this.Description_label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ID_label);
            this.Controls.Add(this.Name_label);
            this.Name = "Payment";
            this.Text = "付款資訊";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.Label ID_label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Description_label;
        private System.Windows.Forms.Label Credit_Card_label;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Print_button;
        private System.Windows.Forms.Button Return_button;
    }
}