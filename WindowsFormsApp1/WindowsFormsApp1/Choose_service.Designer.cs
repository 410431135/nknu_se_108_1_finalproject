﻿namespace WindowsFormsApp1
{
    partial class Choose_service
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Check_In_button = new System.Windows.Forms.Button();
            this.Check_Out_button = new System.Windows.Forms.Button();
            this.Reservation_button = new System.Windows.Forms.Button();
            this.Return_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Check_In_button
            // 
            this.Check_In_button.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Check_In_button.Location = new System.Drawing.Point(298, 96);
            this.Check_In_button.Name = "Check_In_button";
            this.Check_In_button.Size = new System.Drawing.Size(200, 190);
            this.Check_In_button.TabIndex = 1;
            this.Check_In_button.Text = "入房";
            this.Check_In_button.UseVisualStyleBackColor = true;
            this.Check_In_button.Click += new System.EventHandler(this.Check_In_button_Click);
            // 
            // Check_Out_button
            // 
            this.Check_Out_button.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Check_Out_button.Location = new System.Drawing.Point(534, 96);
            this.Check_Out_button.Name = "Check_Out_button";
            this.Check_Out_button.Size = new System.Drawing.Size(200, 190);
            this.Check_Out_button.TabIndex = 2;
            this.Check_Out_button.Text = "退房";
            this.Check_Out_button.UseVisualStyleBackColor = true;
            // 
            // Reservation_button
            // 
            this.Reservation_button.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Reservation_button.Location = new System.Drawing.Point(70, 96);
            this.Reservation_button.Name = "Reservation_button";
            this.Reservation_button.Size = new System.Drawing.Size(200, 190);
            this.Reservation_button.TabIndex = 0;
            this.Reservation_button.Text = "預約";
            this.Reservation_button.UseVisualStyleBackColor = true;
            this.Reservation_button.Click += new System.EventHandler(this.Reservation_button_Click);
            // 
            // Return_button
            // 
            this.Return_button.Location = new System.Drawing.Point(627, 379);
            this.Return_button.Name = "Return_button";
            this.Return_button.Size = new System.Drawing.Size(75, 23);
            this.Return_button.TabIndex = 3;
            this.Return_button.Text = "返回";
            this.Return_button.UseVisualStyleBackColor = true;
            this.Return_button.Click += new System.EventHandler(this.Return_button_Click);
            // 
            // Choose_service
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Return_button);
            this.Controls.Add(this.Check_Out_button);
            this.Controls.Add(this.Check_In_button);
            this.Controls.Add(this.Reservation_button);
            this.Name = "Choose_service";
            this.Text = "選擇服務";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Check_In_button;
        private System.Windows.Forms.Button Check_Out_button;
        private System.Windows.Forms.Button Reservation_button;
        private System.Windows.Forms.Button Return_button;
    }
}