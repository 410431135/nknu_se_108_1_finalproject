﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Confirm_Rooms : Form
    {
        public Confirm_Rooms()
        {
            InitializeComponent();
        }

        private void Confirm_button_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.OK;
        }


        private void Return_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
