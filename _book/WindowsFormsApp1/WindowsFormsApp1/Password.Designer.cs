﻿namespace WindowsFormsApp1
{
    partial class Password
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Discription_label = new System.Windows.Forms.Label();
            this.Name_label = new System.Windows.Forms.Label();
            this.Room_Number_label = new System.Windows.Forms.Label();
            this.Password_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Finish_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Discription_label
            // 
            this.Discription_label.AutoSize = true;
            this.Discription_label.Location = new System.Drawing.Point(36, 39);
            this.Discription_label.Name = "Discription_label";
            this.Discription_label.Size = new System.Drawing.Size(209, 12);
            this.Discription_label.TabIndex = 0;
            this.Discription_label.Text = "以下是您的房間密碼，請牢記，謝謝！";
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(64, 101);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(41, 12);
            this.Name_label.TabIndex = 1;
            this.Name_label.Text = "姓名：";
            // 
            // Room_Number_label
            // 
            this.Room_Number_label.AutoSize = true;
            this.Room_Number_label.Location = new System.Drawing.Point(40, 136);
            this.Room_Number_label.Name = "Room_Number_label";
            this.Room_Number_label.Size = new System.Drawing.Size(65, 12);
            this.Room_Number_label.TabIndex = 2;
            this.Room_Number_label.Text = "房間號碼：";
            // 
            // Password_label
            // 
            this.Password_label.AutoSize = true;
            this.Password_label.Location = new System.Drawing.Point(40, 168);
            this.Password_label.Name = "Password_label";
            this.Password_label.Size = new System.Drawing.Size(65, 12);
            this.Password_label.TabIndex = 3;
            this.Password_label.Text = "房間密碼：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "label3";
            // 
            // Finish_button
            // 
            this.Finish_button.Location = new System.Drawing.Point(114, 294);
            this.Finish_button.Name = "Finish_button";
            this.Finish_button.Size = new System.Drawing.Size(75, 23);
            this.Finish_button.TabIndex = 7;
            this.Finish_button.Text = "完成";
            this.Finish_button.UseVisualStyleBackColor = true;
            this.Finish_button.Click += new System.EventHandler(this.Finish_button_Click);
            // 
            // Password
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 393);
            this.Controls.Add(this.Finish_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Password_label);
            this.Controls.Add(this.Room_Number_label);
            this.Controls.Add(this.Name_label);
            this.Controls.Add(this.Discription_label);
            this.Name = "Password";
            this.Text = "入房成功";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Discription_label;
        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.Label Room_Number_label;
        private System.Windows.Forms.Label Password_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Finish_button;
    }
}