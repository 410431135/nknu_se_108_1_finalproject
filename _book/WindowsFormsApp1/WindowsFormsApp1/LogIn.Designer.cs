﻿namespace WindowsFormsApp1
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.姓名_label = new System.Windows.Forms.Label();
            this.ID_label = new System.Windows.Forms.Label();
            this.姓名_textBox = new System.Windows.Forms.TextBox();
            this.身分證字號_textBox = new System.Windows.Forms.TextBox();
            this.Next_button = new System.Windows.Forms.Button();
            this.Return_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 姓名_label
            // 
            this.姓名_label.AutoSize = true;
            this.姓名_label.Location = new System.Drawing.Point(148, 114);
            this.姓名_label.Name = "姓名_label";
            this.姓名_label.Size = new System.Drawing.Size(29, 12);
            this.姓名_label.TabIndex = 0;
            this.姓名_label.Text = "姓名";
            // 
            // ID_label
            // 
            this.ID_label.AutoSize = true;
            this.ID_label.Location = new System.Drawing.Point(112, 174);
            this.ID_label.Name = "ID_label";
            this.ID_label.Size = new System.Drawing.Size(65, 12);
            this.ID_label.TabIndex = 1;
            this.ID_label.Text = "身分證字號";
            // 
            // 姓名_textBox
            // 
            this.姓名_textBox.Location = new System.Drawing.Point(183, 111);
            this.姓名_textBox.Name = "姓名_textBox";
            this.姓名_textBox.Size = new System.Drawing.Size(341, 22);
            this.姓名_textBox.TabIndex = 2;
            // 
            // 身分證字號_textBox
            // 
            this.身分證字號_textBox.Location = new System.Drawing.Point(183, 171);
            this.身分證字號_textBox.Name = "身分證字號_textBox";
            this.身分證字號_textBox.Size = new System.Drawing.Size(341, 22);
            this.身分證字號_textBox.TabIndex = 3;
            // 
            // Next_button
            // 
            this.Next_button.Location = new System.Drawing.Point(540, 334);
            this.Next_button.Name = "Next_button";
            this.Next_button.Size = new System.Drawing.Size(75, 23);
            this.Next_button.TabIndex = 4;
            this.Next_button.Text = "下一步";
            this.Next_button.UseVisualStyleBackColor = true;
            this.Next_button.Click += new System.EventHandler(this.Next_button_Click);
            // 
            // Return_button
            // 
            this.Return_button.Location = new System.Drawing.Point(417, 334);
            this.Return_button.Name = "Return_button";
            this.Return_button.Size = new System.Drawing.Size(75, 23);
            this.Return_button.TabIndex = 5;
            this.Return_button.Text = "返回";
            this.Return_button.UseVisualStyleBackColor = true;
            this.Return_button.Click += new System.EventHandler(this.Return_button_Click);
            // 
            // LogIn_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Return_button);
            this.Controls.Add(this.Next_button);
            this.Controls.Add(this.身分證字號_textBox);
            this.Controls.Add(this.姓名_textBox);
            this.Controls.Add(this.ID_label);
            this.Controls.Add(this.姓名_label);
            this.Name = "LogIn_Form";
            this.Text = "登入";
            this.Load += new System.EventHandler(this.Start_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label 姓名_label;
        private System.Windows.Forms.Label ID_label;
        private System.Windows.Forms.TextBox 姓名_textBox;
        private System.Windows.Forms.TextBox 身分證字號_textBox;
        private System.Windows.Forms.Button Next_button;
        private System.Windows.Forms.Button Return_button;
    }
}