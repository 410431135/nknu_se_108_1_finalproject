﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class 歡迎使用 : Form
    {
        public 歡迎使用()
        {
            InitializeComponent();
        }

        private void 開始_button_Click(object sender, EventArgs e)
        {
            LogIn lf = new LogIn();
            this.Hide();
            if (lf.ShowDialog() == DialogResult.OK)
            {
                this.Show();
            }
        }


        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Register rg = new Register();
            rg.ShowDialog(this);
        }
    }
}
