﻿namespace WindowsFormsApp1
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_label = new System.Windows.Forms.Label();
            this.ID_label = new System.Windows.Forms.Label();
            this.Address_label = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.確認_button = new System.Windows.Forms.Button();
            this.取消_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(121, 111);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(29, 12);
            this.Name_label.TabIndex = 0;
            this.Name_label.Text = "姓名";
            // 
            // ID_label
            // 
            this.ID_label.AutoSize = true;
            this.ID_label.Location = new System.Drawing.Point(121, 140);
            this.ID_label.Name = "ID_label";
            this.ID_label.Size = new System.Drawing.Size(65, 12);
            this.ID_label.TabIndex = 1;
            this.ID_label.Text = "身分證字號";
            // 
            // Address_label
            // 
            this.Address_label.AutoSize = true;
            this.Address_label.Location = new System.Drawing.Point(121, 168);
            this.Address_label.Name = "Address_label";
            this.Address_label.Size = new System.Drawing.Size(29, 12);
            this.Address_label.TabIndex = 2;
            this.Address_label.Text = "地址";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(211, 101);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(318, 22);
            this.textBox1.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(211, 130);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(318, 22);
            this.textBox2.TabIndex = 4;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(211, 157);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(318, 22);
            this.textBox3.TabIndex = 5;
            // 
            // 確認_button
            // 
            this.確認_button.Location = new System.Drawing.Point(546, 288);
            this.確認_button.Name = "確認_button";
            this.確認_button.Size = new System.Drawing.Size(75, 23);
            this.確認_button.TabIndex = 6;
            this.確認_button.Text = "確認";
            this.確認_button.UseVisualStyleBackColor = true;
            this.確認_button.Click += new System.EventHandler(this.確認_button_Click);
            // 
            // 取消_button
            // 
            this.取消_button.Location = new System.Drawing.Point(431, 288);
            this.取消_button.Name = "取消_button";
            this.取消_button.Size = new System.Drawing.Size(75, 23);
            this.取消_button.TabIndex = 7;
            this.取消_button.Text = "取消";
            this.取消_button.UseVisualStyleBackColor = true;
            this.取消_button.Click += new System.EventHandler(this.取消_button_Click);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.取消_button);
            this.Controls.Add(this.確認_button);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Address_label);
            this.Controls.Add(this.ID_label);
            this.Controls.Add(this.Name_label);
            this.Name = "Register";
            this.Text = "註冊";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.Label ID_label;
        private System.Windows.Forms.Label Address_label;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button 確認_button;
        private System.Windows.Forms.Button 取消_button;
    }
}