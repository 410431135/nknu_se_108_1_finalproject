﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Choose_Room : Form
    {
        public Choose_Room()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool smoke = checkBox1.Checked;
            if (smoke)
            {
                button2.Enabled=false;
            }
            else
            {
                button2.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm_Rooms cf = new Confirm_Rooms();
            cf.ShowDialog();
            if (cf.DialogResult == DialogResult.OK)
            {
                this.Hide();
                Payment pm = new Payment();
                pm.ShowDialog();
                if (pm.DialogResult == DialogResult.OK)
                {
                    this.Show();
                }
            }
        }

        private void 返回_button_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }
    }
}
