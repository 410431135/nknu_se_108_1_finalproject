﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Check_In : Form
    {
        public Check_In()
        {
            InitializeComponent();
        }

        private void Next_button_Click(object sender, EventArgs e)
        {
            Password ps = new Password();
            ps.ShowDialog();
            if (ps.DialogResult == DialogResult.OK)
            {
                this.Close();
                
            }
        }
    }
}
