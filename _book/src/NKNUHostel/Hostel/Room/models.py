from django.db import models
from django.contrib.auth.models import User

class Room(models.Model):
	room_id = models.IntegerField(verbose_name='房間號碼', primary_key=True)
	room_price = models.FloatField(verbose_name='定價', blank=True)
	isSmokeRoom = models.BooleanField(verbose_name='吸菸房', blank=True, default=False)
	occupying_costumer = models.ForeignKey(User, verbose_name='使用中客戶', blank=True, null=True, on_delete=models.SET_NULL)
	start_occupied_date = models.DateField(verbose_name='入住時間', blank=True, null=True)
	end_occupied_date = models.DateField(verbose_name='退房時間', blank=True, null=True)
	isConnected = models.BooleanField(verbose_name='比鄰它房', default=True)
	dobule_beds = models.IntegerField(verbose_name='雙人床數量', default=0)
	single_beds = models.IntegerField(verbose_name='單人床數量', default=0)
	introduction = models.TextField(verbose_name='簡介', blank=True)
	hasView = models.BooleanField(verbose_name='景觀房', default=False)
	password = models.CharField(verbose_name='房間密碼', max_length=8, null=True, blank=True)

	def __str__(self):
		return '%s 號房' % self.room_id