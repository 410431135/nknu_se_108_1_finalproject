from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import random
from .models import Room
from .forms import PayForm

def generate_password(length):
	password = ''
	for _ in range(length):
		password += str(random.randint(0,9))
	return password

def start(request):
	return render(request, 'Room/start.html', locals())

@login_required
def main(request):
	user = request.user
	return render(request, 'Room/main.html', locals())

@login_required
def reservation(request):
	room_list = Room.objects.all()
	return render(request, 'Room/browse_room.html', locals())

@login_required
def room_detail(request, room_id):
	room = Room.objects.get(room_id = room_id)
	return render(request, 'Room/room_detail.html', locals())

@login_required
def pay(request, room_id):
	room = Room.objects.get(room_id = room_id)
	user = request.user
	discount = '%.2f' % (room.room_price*0.8)
	form = PayForm(request.POST)
	if request.method == 'POST' and request.POST.get('security'):
		security = request.POST.get('security')
		try:
			int(security) % 1000 == int(security)
		except Exception as e:
			return HttpResponse('87哦別亂打')
		if form.is_valid():
			room.occupying_costumer = user
			room.start_occupied_date = form.cleaned_data['start_occupied_date']
			room.end_occupied_date = form.cleaned_data['end_occupied_date']
			room.save()
			return HttpResponse('交易成功!<br><a href="/main">回主畫面</a>')
	return render(request, 'Room/pay.html', locals())

@login_required
def checkin(request):
	user = request.user
	# 確認是否有預約
	isReserved = False
	# 若有
	if len(user.room_set.all()) != 0:
		isReserved = True
		room = user.room_set.all()[0]
		room_number = room.room_id
		room_password = room.password
		start = room.start_occupied_date
		end = room.end_occupied_date
		
		# 重新產生房間密碼
		if request.method == 'POST' and request.POST.get('change'):
			new_password = generate_password(6)
			room.password = new_password
			room.save()
			return redirect('/checkin')

	return render(request, 'Room/checkin.html', locals())

@login_required
def checkout(request):
	user = request.user
	try:
		room = Room.objects.filter(occupying_costumer = user)[0]
	except Exception as e:
		return HttpResponse('還沒訂房是要退什麼啦')
	discount = '%.2f' % (room.room_price*0.8)
	return render(request, 'Room/checkout.html', locals())

@login_required
def bill(request):
	user = request.user
	room = Room.objects.filter(occupying_costumer = user)[0]
	room.occupying_costumer = None
	room.save()
	return HttpResponse('嗶嗶嗶.. 列印中.. 請在下方取出您的帳單...<br> 本帳單僅供紀錄用... <br> <a href="/user/logout">點我登出</a>')


