from django.db import models
from django.contrib.auth.models import User


class UserInfo(models.Model):
	user = models.OneToOneField(User, verbose_name='顧客', on_delete=models.CASCADE)
	credit_card_number = models.CharField(max_length=32, verbose_name='信用卡卡號', blank=True, null=True)
	address = models.TextField(verbose_name='地址', blank=True)
	phone_number = models.CharField(verbose_name='電話', max_length=32, blank=True)
