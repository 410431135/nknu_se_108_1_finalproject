from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import User
from django import forms
from user.forms import RegisterForm, LoginForm
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils import timezone
from .models import UserInfo




def register(request):
	# upon user send the register information
	if request.method == "POST":
		registerForm = RegisterForm(request.POST)

		errors = []
		if not registerForm.is_valid():
			errors.extend(registerForm.errors.values())
			return render(request, 'user/user_register.html', locals())

		else:
			username = registerForm.cleaned_data['username']
			password1 = registerForm.cleaned_data['password1']
			password2 = registerForm.cleaned_data['password2']
			email = registerForm.cleaned_data['email']
			credit_card_number = registerForm.cleaned_data['credit_card_number']
			address = registerForm.cleaned_data['address']
			phone_number = registerForm.cleaned_data['phone_number']

		# verify the length of the password
		if (len(password1) < 8):
			errors.append('password too short.')
			return render(request, 'user/user_register.html', locals())
		# verify if password correct
		if password1 != password2:
			errors.append('password not same.')
			return render(request, 'user/user_register.html', locals())

		# verify if exist same user
		filterSameUser = User.objects.filter(username = username)
		if (len(filterSameUser) > 0):
			errors.append('username exits.')
			return render(request, 'user/user_register.html', locals())

		# if registeration vaild
		user = User()
		user.username = username
		user.set_password(password1)
		user.email = email
		user.save()
		info = UserInfo.objects.create(
				user=user,
				credit_card_number=credit_card_number,
				address=address,
				phone_number=phone_number,
			)
		info.save()
		# verify new user
		newUser = auth.authenticate(username = username, password = password1)
		if newUser is not None:
			auth.login(request, newUser)
			return HttpResponseRedirect('/')

	registerForm = RegisterForm(request.POST)
	return render(request, 'user/user_register.html', locals())


def login(request):
	# if user come from somewhere
	nextValue = request.POST.get('next')
	# if user has already logged in
	if request.user.is_authenticated:
		return HttpResponse('預約、入住、退房')
	if request.method == "POST":
		loginForm = LoginForm(request.POST)
		errors =''
		if loginForm.is_valid():
			username = loginForm.cleaned_data['username']
			password = loginForm.cleaned_data['password']
			user = auth.authenticate(username = username, password = password)
			if user and user.is_active:
				auth.login(request, user)
				if nextValue != '':
					return redirect(nextValue)
				else:
					return HttpResponse('預約、入住、退房')
			else:
				errors = "Account or password is wrong."
				# if poassword wrong but from otherwhere
				# this is not a good approach cos redirect twice and no errors message
				if nextValue != '':
					return redirect(nextValue)
				else:
					return render(request, 'user/user_login.html', locals())
	loginForm = LoginForm(request.POST)
	return render(request, 'user/user_login.html', locals())

# this shall not exist, maybe is to delete someday
def logout(request):
	auth.logout(request)
	return HttpResponse("logout successfully <br> <a href=\"/\">回開始畫面</a> ")

