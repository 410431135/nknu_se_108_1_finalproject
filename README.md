# 軟體工程期末專題

## About

108-1軟體工程期末專題 -- **高師大旅館**

### 開發環境

~~`c#` `Windows Form applications (.Net freamwork)`~~

`python 3.6.3 + django 2.2.6`

### 文件

[Gitbook](https://410431135.gitlab.io/nknu_se_108_1_finalproject/)

### 版本控制

[Gitlab](https://gitlab.com/410431135/nknu_se_108_1_finalproject)

## 指導老師

葉道明 教授

## Authors

**林冠曄**

- 高雄國立師範大學 數學系數學組108級

**彭奎量**

- 高雄國立師範大學 軟體工程與管理系

**李洋昊**

- 高雄國立師範大學 軟體工程與管理系

## Gantt Chart

[甘特圖](https://docs.google.com/spreadsheets/d/1Gduqa2QIkRM8djKh8N1jA14YIpTEn1qlLglUecEhrDw/edit?usp=sharing)

![甘特圖](gantt_2019_12_03.png)

