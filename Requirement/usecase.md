# 使用案例圖 Use Case Diagrams

## Description

請依下列使用者需求，進一步收集與分析功能與非功能需求後，以**使用案例**形式撰寫制定系統需求規格，規格內容要**確實、完整、一致、可驗證**。

該專案的目標是建造一個軟體系統來管理**高師大旅館**的”**前台活動**。

你與客戶已經簽約，你的客戶相信，取代現有紙本作業的自動化系統將節省成本，並幫助他們為客人提供更好服務。該系統將用於處理**預約訂房資料以及客人入住與離開之登記作業**。

該旅館有30間客房，可供客人入住。有些旅館房間毗鄰其他房間; 即房間之間存在著內部相通的門，因此它們可以單獨使用或作為套房。

客人可以住在任何一間單獨房間或套房。

每個旅館客房都分配有一個質量水準（例如，一個較大的房間或有景觀的房間，會比一個沒有景觀的小房間更好）。每間客房有一定的床位數量和類型、房號、和吸煙/非吸煙狀況。

每一個質量水準都有一日最高定價，儘管客人實際支付費用可能比較低。 

當旅館客人要預約，旅館職員問他/她想住哪幾天，以及想要的房間類型。

該系統必須驗證那段期間有空房，才可允許預約。 

旅館需要記錄每個客人的基本資料，如他/她的姓名、地址、電話號碼，信用卡等。

預約可以隨時取消。 

當客人入住時，某間房要分配給他/她，直到他/她結帳離開。

該系統必須記錄該客人的賬戶，並能列印他/她的帳單。

## Diagrams

```uml
@startuml
actor 客戶 as user
actor 職員 as staff
actor 系統 as sys

(預約訂房) as reserve
(入住登記) as checkin
(離開登記) as checkout
(登記作業) as login <<essential>>
(選擇房型) as chose
(驗證空房) as verify
(記錄客人資料) as record
(取消預約) as cancel
(記錄帳戶) as recordBank
(列印帳單) as print
(管理房間) as manage

user -down- reserve
chose -> reserve : <<include>>
user - login
login <|- checkin : <<generalize>>
login <|- checkout : <<generalize>>
chose -down-> verify : <<include>>
staff - record
record -down- sys
record -> recordBank : <<include>>
sys - recordBank
recordBank <.right. print : <<extend>>
reserve <.. cancel : <<extend>>
reserve - staff
manage -up- staff
login - staff

@enduml
```



## Use Case Description 

| 預約訂房    |                                                      |
| ----------- | ---------------------------------------------------- |
| Actor       | 客戶、職員                                           |
| Description | 職員詢問客戶住哪幾天、想要的房型。若有空房才可預約。 |
| Data        | Retrieve 房型 available  or not in specific date.    |
| Simulate    | 職員輸入資料                                         |
| Response    | 房型資料 up to date.                                 |
| Note        | 需紀錄客戶資料                                       |



| 紀錄客人資料 |                                                              |
| ------------ | ------------------------------------------------------------ |
| Actor        | 客戶、職員、系統                                             |
| Description  | 職員需要記錄每個客人的基本資料，   如他/她的姓名、地址、電話號碼，信用卡等。 |
| Data         | Create 客戶資料                                              |
| Simulate     | 職員輸入                                                     |
| Response     | Create successfully or not.                                  |
| Note         | System should record customer’s account.                     |



| 取消預約    |                                             |
| ----------- | ------------------------------------------- |
| Actor       | 客戶、職員、系統                            |
| Description | 客戶欲取消預約                              |
| Data        | Delete data of reservation of the customer. |
| Simulate    | 客戶提出取消                                |
| Response    | Confirm that the reservation was canceled   |
| Note        | Account in system should be deleted too.    |



| 登記作業    |                                                              |
| ----------- | ------------------------------------------------------------ |
| Actor       | 職員                                                         |
| Description | 職員在客戶入住時辦理入住登記、離開時辦理離開登記             |
| Data        | Update status to 入住/離開。房間管理update at the same time. |
| Simulate    | 客戶入住                                                     |
| Response    | 登記成功                                                     |
| Note        | Staff should check 房間 status。                             |



| 管理房間    |                                                  |
| ----------- | ------------------------------------------------ |
| Actor       | 職員                                             |
| Description | 房間狀態有異動時進行更新                         |
| Data        | Update status of the room.                       |
| Simulate    | Upon any change of the room.                     |
| Response    | Updated.                                         |
| Note        | Used for checking reservation available or  not. |



| 列印帳單    |                                                              |
| ----------- | ------------------------------------------------------------ |
| Actor       | 職員、系統                                                   |
| Description | 於客戶離開登記時，列印帳單                                   |
| Data        | Retrieve account of the customer and  calculate and print the bill. |
| Simulate    | 離開登記                                                     |
| Response    | Bill printed                                                 |
| Note        |                                                              |