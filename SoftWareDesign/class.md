# Class Diagram 類別圖
##v1.0
```uml
@startuml
skinparam classAttributeIconSize 0
class Costumer{
- costumer_id : int
- name : String
- address : String
- mobile : String
- credit_card_number : String

void register()
void login()
void logout()
}

class Room{
- room_id : int
- dobule_beds : int
- single_beds : int
- isSmokeRoom : boolean
- room_price : float
- start_occupied_date : date
- end_occupied_date : date
- isConnected : boolean
- introduction : String
- hasView  : boolean
- password : String

checkin(request)
checkout(request)
bill(request)
cancel(request)
}

class reservation{
- room_list : query_set
}
class room_detail{
- room : room_object
}
class pay{
- room : room_object
- user : User_object
- discount : float
}
class PayForm{
# start_occupied_date : date
# end_occupied_date : date
}
reservation -> Room : obtain information >
reservation <- room_detail : obtain detail <
reservation <- pay
pay --o PayForm : use >
Costumer "1" --> "0..1" Room : occupy >
@enduml
```

##v0.1
```uml
@startuml
skinparam classAttributeIconSize 0
class Costumer{
- costumer_id : int
- name : String
- address : String
- mobile : String
- credit_card_number : String

void register()
void login()
void logout()
}

class Room{
- room_id : int
- dobule_beds : int
- single_beds : int
- isSmokeRoom : boolean
- room_price : float
- start_occupied_date : date
- end_occupied_date : date
- isConnected : boolean
- introduction : String
- hasView  : boolean
- password : String

String generate_password(length)
start(request)
main(request)
reservation(request)
room_detail(request, room_id)
pay(request)
checkin(request)
checkout(request)
bill(request)
}
Costumer "1" --> "0..1" Room : occupy >
@enduml
```

