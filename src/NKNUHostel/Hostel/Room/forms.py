from django import forms

class PayForm(forms.Form):
	start_occupied_date = forms.DateField(label = '入住日期', widget = forms.SelectDateWidget())
	end_occupied_date = forms.DateField(label = '退房日期', widget = forms.SelectDateWidget())