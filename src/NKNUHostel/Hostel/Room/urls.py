from django.urls import path
from . import views

urlpatterns = [
	path('', views.start, name='start'),
	path('main', views.main, name='main'),
	path('reservation', views.reservation, name='reservation'),
	path('checkin', views.checkin, name='check in'),
	path('checkout', views.checkout, name='check out'),
	path('room/<int:room_id>', views.room_detail, name='detail'),
	path('pay/<int:room_id>', views.pay, name='pay'),
	path('bill', views.bill, name='print bill'),
]