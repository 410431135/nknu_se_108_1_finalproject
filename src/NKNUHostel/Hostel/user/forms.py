from django import forms
from user.models import *

class RegisterForm(forms.Form):
	username = forms.CharField(label = "使用者名稱", max_length = 128, widget = forms.TextInput)
	password1 = forms.CharField(label = "密碼(大於8位)", max_length = 256, widget = forms.PasswordInput)
	password2 = forms.CharField(label = "確認密碼", max_length = 256, widget = forms.PasswordInput)
	email = forms.EmailField(label = "電子信箱", max_length = 256, widget = forms.EmailInput)
	credit_card_number = forms.CharField(label='信用卡卡號', max_length=32)
	address = forms.CharField(label='地址')
	phone_number = forms.CharField(label='電話', max_length=32)

class LoginForm(forms.Form):
	username = forms.CharField(label = "使用者名稱", max_length = 128, widget = forms.TextInput)
	password = forms.CharField(label = "密碼", max_length = 256, widget = forms.PasswordInput)

		